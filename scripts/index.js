//SERVICES
const services = document.querySelector(".services")
const list = document.querySelector(".services__list");
const servises = [...list.children];
const ourTexts = document.querySelector(".tabs__content");
const text = [...ourTexts.children];
const aboutImages = document.querySelector(".about__img");
const aboutImagesArr = [...aboutImages.children];

// WORKS
const wokrsList = document.querySelector(".works__list");
const worksListArr = [...wokrsList.children];
const workImg = document.querySelector(".works__images");
const workImgArr = [...workImg.children];
const worcContainer = document.querySelector(".works__images-container");
const greenArrow = document.querySelectorAll(".arrow");
const greenArrowArr = [...greenArrow];

greenArrowArr.forEach(arrow => {
  arrow.style.display = "none"
})
// OUR SERVICES
text.forEach((text) => {
  text.style.display = "none";
});

aboutImagesArr.forEach((img) => {
  img.style.display = "none";
});

showText(servises, text, aboutImagesArr, greenArrowArr);


function showText(element, story, images, arrows) {
  let sss;
  for (const el of element) {
    el.addEventListener("click", function () {
      
      element.forEach((elem) => {
        sss = elem;
        sss.classList.remove("active");
      });
      el.dataset.find = el.innerText;
      el.classList.add("active");
      for (const e of story) {
        if (e.dataset.find === el.dataset.find) {
            e.style.display = "block";
            } else {
          e.style.display = "none";
        }
      }
      for (const imgage of images) {
        if (imgage.dataset.find === el.dataset.find) {
          imgage.style.display = "flex";
        } else {
          imgage.style.display = "none";
        }
      }
      for (const ar of arrows) {
        if (ar.dataset.find === el.dataset.find) {
          ar.style.display = "block";
        } else {
          ar.style.display = "none";
        }
      }
    });
  }
}

// AMAZING WORKS
const carDesign = [
  "https://static.dezeen.com/uploads/2020/03/hyundai-prophecy-car-design_dezeen_2364_col_14-852x565.jpg",
  "https://c.files.bbci.co.uk/2F81/production/_103916121_mediaitem103910950.jpg",
  "https://d1vl6ykwv1m2rb.cloudfront.net/blog/wp-content/uploads/2020/03/03132803/2.jpg",
  "https://d2hucwwplm5rxi.cloudfront.net/wp-content/uploads/2021/05/04110012/What-Are-Concept-Cars-Cover-040520211552.jpg",
  "https://www.mercedes-benz.com/en/vehicles/passenger-cars/concept-cars/_jcr_content/root/slider_copy/sliderchilditems/slideritem_copy/image/MQ7-0-image-20210517163818/01-mercedes-benz-eq-concept-cars-3400x1440.jpeg",
  "https://hips.hearstapps.com/roa.h-cdn.co/assets/16/04/1453903559-opel-gt-concept-1.jpg?crop=0.840xw:0.840xh;0,0.160xh&resize=1200:*",
  "https://supercarblondie.com/wp-content/uploads/Maserati-Sailand.jpg",
  "https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/large-50498-hyundaimotorsnbrandunveilstworollinglabconceptssignalinghigh-performancevisionforelectrificationera-1658155683.jpg",
  "https://api.time.com/wp-content/uploads/2014/12/06-top-concept-cars.jpg",
  "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTWQMRwxGqcBdQaAHv1CsrfT6N5HVr9SVkfjQ&usqp=CAU",
  "https://www.hyundai.co.kr/image/upload/asset_library/MDA00000000000017445/d30fa46cc03d4667afdfac9c44048011.jpg",
  "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQOIaerTS8COp_dOZjbpShVNSmza6sVM9817Q&usqp=CAU",
];

const personagesSrc = [
  "https://i.playground.ru/p/UX-LVXhHyk3bw7xLQ_SP_A.jpeg",
  "https://i.pinimg.com/564x/ea/0a/3a/ea0a3a33304c01288a1069567c7482d9.jpg",
  "https://i.pinimg.com/736x/27/e4/d2/27e4d2bc97a799b30c55a8ecaf1cdc1a.jpg",
  "https://i.playground.ru/p/UX-LVXhHyk3bw7xLQ_SP_A.jpeg",
  "https://i.pinimg.com/564x/ea/0a/3a/ea0a3a33304c01288a1069567c7482d9.jpg",
  "https://i.pinimg.com/736x/27/e4/d2/27e4d2bc97a799b30c55a8ecaf1cdc1a.jpg",
  "https://i.playground.ru/p/UX-LVXhHyk3bw7xLQ_SP_A.jpeg",
  "https://i.pinimg.com/564x/ea/0a/3a/ea0a3a33304c01288a1069567c7482d9.jpg",
  "https://i.pinimg.com/736x/27/e4/d2/27e4d2bc97a799b30c55a8ecaf1cdc1a.jpg",
  "https://i.playground.ru/p/UX-LVXhHyk3bw7xLQ_SP_A.jpeg",
  "https://i.pinimg.com/564x/ea/0a/3a/ea0a3a33304c01288a1069567c7482d9.jpg",
  "https://i.pinimg.com/736x/27/e4/d2/27e4d2bc97a799b30c55a8ecaf1cdc1a.jpg",
];

const backgroundsSrc = [
  "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__340.jpg",
  "https://i.pinimg.com/originals/4b/de/74/4bde743be41f7e2220e5b180078c7d43.jpg",
  "https://c4.wallpaperflare.com/wallpaper/39/346/426/digital-art-men-city-futuristic-night-hd-wallpaper-preview.jpg",
  "https://cdn.pixabay.com/photo/2020/12/20/21/17/city-5848267__340.jpg",
  "https://cdn.pixabay.com/photo/2016/05/05/02/37/sunset-1373171__480.jpg",
  "https://static-cse.canva.com/blob/572626/1.magebyRodionKutsaevviaUnsplash.jpg",
  "https://media.istockphoto.com/id/1336074248/photo/blue-christmas-and-new-year-holiday-frame.jpg?b=1&s=170667a&w=0&k=20&c=BQHrKhSk2CU54Pduw4ss9ObCqfoW2jLPEwi2T-5hAp4=",
  "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__340.jpg",
  "https://i.pinimg.com/originals/4b/de/74/4bde743be41f7e2220e5b180078c7d43.jpg",
  "https://c4.wallpaperflare.com/wallpaper/39/346/426/digital-art-men-city-futuristic-night-hd-wallpaper-preview.jpg",
  "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTmpfMGCfU0frSa4xsp31m5irELqnlZ_U-5Jg&usqp=CAU",
  "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcStftx_n_ck5zAKEAd1cyPQdIGc02PE_2hlBg&usqp=CAU",
];

const presentationSrc = [
  "https://www.slideegg.com/image/catalog/87934-Cyberpunk_PowerPoint_Template.png",
  "https://i.pinimg.com/originals/00/6f/9d/006f9dbcf912f65e0c545a95fbc074ce.jpg",
  "https://pic.pptdaily.com/powerpoint/134545/cyberpunk-game-ppt-purple-1-purple_1.jpg!fw850pg",
  "https://s3.envato.com/files/345319452/Car%20Presentation%201920x1080.jpg",
  "https://i.pinimg.com/originals/d5/5b/60/d55b60f37aeb0dc232538069a2c015fb.jpg",
  "https://imgscf.slidemembers.com/docs/1/1/764/the_car_powerpoint_presentation_ppt_763814.jpg",
  "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQzsJD52RLpYicPACoCOApMQ_lukA_DB9YdIA&usqp=CAU",
  "https://i.pinimg.com/originals/00/6f/9d/006f9dbcf912f65e0c545a95fbc074ce.jpg",
  "https://pic.pptdaily.com/powerpoint/134545/cyberpunk-game-ppt-purple-1-purple_1.jpg!fw850pg",
  "https://s3.envato.com/files/345319452/Car%20Presentation%201920x1080.jpg",
  "https://i.pinimg.com/originals/d5/5b/60/d55b60f37aeb0dc232538069a2c015fb.jpg",
  "https://imgscf.slidemembers.com/docs/1/1/764/the_car_powerpoint_presentation_ppt_763814.jpg",
];

const allImg = [carDesign, personagesSrc, backgroundsSrc, presentationSrc];

const allImgArr = [
  ...carDesign,
  ...personagesSrc,
  ...backgroundsSrc,
  ...presentationSrc,
];
workImg.style.display = "none";

showImg(worksListArr, allImg);

function showImg(tabs, images) {
  tabs.forEach((t) => {
    t.addEventListener("click", function () {
      tabs.forEach((t) => {
        
        t.classList.remove("active");
        workImg.innerHTML = "";
        loadBtn.style.display = "flex";
      });
      t.dataset.work = t.innerText;
      for (const car of images[0]) {
        const img = document.createElement("img");
        img.src = car;
        img.classList.add("work__image");
        img.dataset.work = "Car Design";
        if (t.dataset.work === "Car Design") {
          t.classList.add("active");
          img.style.display = "block";
          if (workImg.childNodes.length < 12) {
            workImg.style.display = "grid";
            workImg.appendChild(img);
          }
        }
      }
      for (const persone of images[1]) {
        const img = document.createElement("img");
        img.src = persone;
        img.classList.add("work__image");
        img.dataset.work = "Personajes";
        if (t.dataset.work === "Personajes") {
          t.classList.add("active");
          img.style.display = "block";
          if (workImg.childNodes.length < 12) {
            workImg.style.display = "grid";
            workImg.appendChild(img);
          }
        }
      }
      for (const background of images[2]) {
        const img = document.createElement("img");
        img.src = background;
        img.classList.add("work__image");
        img.dataset.work = "Backgrounds";
        if (t.dataset.work === "Backgrounds") {
          t.classList.add("active");
          img.style.display = "block";
          if (workImg.childNodes.length < 12) {
            workImg.style.display = "grid";
            workImg.appendChild(img);
          }
        }
      }
      for (const presentation of images[3]) {
        const img = document.createElement("img");
        img.src = presentation;
        img.classList.add("work__image");
        img.dataset.work = "Presentation";
        if (t.dataset.work === "Presentation") {
          t.classList.add("active");
          img.style.display = "block";
          if (workImg.childNodes.length < 12) {
            workImg.style.display = "grid";
            workImg.appendChild(img);
          }
        }
      }
      for (const all of images) {
        all.forEach((e, index) => {
          const img = document.createElement("img");
          img.src = allImgArr[Math.floor(Math.random() * allImgArr.length)];
          img.classList.add("work__image");
          img.dataset.work = "All";
          if (t.dataset.work === "All") {
            t.classList.add("active");
            img.style.display = "block";
            if (workImg.childNodes.length < 12) {
              workImg.style.display = "grid";
              workImg.appendChild(img);
            }
          }
        });
      }
    });
  });
}

//

// LOAD MORE BUTTON
const loadBtn = document.querySelector(".load__more");
loadBtn.addEventListener("click", (evt) => {
  evt.target.classList.add("progruzka");
  setTimeout(() => {
    evt.target.classList.remove("progruzka");
  }, 1500);
  setTimeout(() => {
    for (const car of allImg[0]) {
      const img = document.createElement("img");
      img.src = car;
      img.classList.add("work__image");
      img.dataset.work = "Car Design";
      const sss = [...workImg.childNodes];
      const nnn = [...sss];
      nnn.forEach((e) => {
        if (e.dataset.work === img.dataset.work) {
          workImg.appendChild(img);
          loadBtn.style.display = "none";
        }
      });
    }
    for (const persone of allImg[1]) {
      const img = document.createElement("img");
      img.src = persone;
      img.classList.add("work__image");
      img.dataset.work = "Personajes";
      const sss = [...workImg.childNodes];
      const nnn = [...sss];
      nnn.forEach((e) => {
        if (e.dataset.work === img.dataset.work) {
          workImg.appendChild(img);
          loadBtn.style.display = "none";
        }
      });
    }

    for (const background of allImg[2]) {
      const img = document.createElement("img");
      img.src = background;
      img.classList.add("work__image");
      img.dataset.work = "Backgrounds";
      const sss = [...workImg.childNodes];
      const nnn = [...sss];
      nnn.forEach((e) => {
        if (e.dataset.work === img.dataset.work) {
          workImg.appendChild(img);
          loadBtn.style.display = "none";
        }
      });
    }
    for (const present of allImg[3]) {
      const img = document.createElement("img");
      img.src = present;
      img.classList.add("work__image");
      img.dataset.work = "Presentation";
      const sss = [...workImg.childNodes];
      const nnn = [...sss];
      nnn.forEach((e) => {
        if (e.dataset.work === img.dataset.work) {
          workImg.appendChild(img);
          loadBtn.style.display = "none";
        }
      });
    }
    for (const all of allImg) {
      while (workImg.childNodes.length < 24) {
        const img = document.createElement("img");
        img.src = allImgArr[Math.floor(Math.random() * allImgArr.length)];
        img.classList.add("work__image");
        img.dataset.work = "All";
        const sss = [...workImg.childNodes];
        const nnn = [...sss];
        nnn.forEach((e) => {
          if (e.dataset.work === img.dataset.work) {
            workImg.appendChild(img);
            loadBtn.style.display = "none";
          }
        });
      }
    }
  }, 1500);
});

// Breaking News

const day = document.querySelectorAll(".day");
const dayArr = [...day];
const month = document.querySelectorAll(".month");
const monthArr = [...month];

const allMonthsArr = [
  "Jun",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec",
];

dayArr.forEach((e) => {
  e.innerHTML = Math.floor(Math.random() * 29 + 1);
});

monthArr.forEach((e) => {
  e.innerHTML = allMonthsArr[Math.floor(Math.random() * allMonthsArr.length)];
});

// SLIDER
const sss = document.querySelector(".active__persone");
const slider = document.querySelectorAll(".slide__single");
const sliderArr = [...slider];
const activePersone = document.querySelector(".activ__persone");
const rightBtn = document.querySelector(".tap__tapp");
const lefttBtn = document.querySelector(".tap__tap");
const prf = document.querySelector(".prof");
const prof = ["Ux Designer", "Front-End Developer", "SEO", "HR"];
const personeName = document.querySelector(".name")
const personeNames = ["LANA DEL REY",
"SKRIPTONIT",
"ANTON PTYSHKIN",
"BELLA MURE"]
const personeText = document.querySelector(".preferences");
const personeTexts = [
"Avocados are a fruit, not a vegetable. They're technically considered a single-seeded berry, believe it or not.",
"The Chupa Chups logo was designed by Salvador Dalí. The surrealist artist designed the logo in 1969.",
"Ketchup was once sold as medicine. The condiment was prescribed and sold to people suffering with indigestion back in 1834.",
"The moon has moonquakes. They happen due to tidal stresses connected to the distance between the moon and the Earth.",
]
let counter = 0;


sss.style.transform = "scale(100%)";




for (let i = 0; i < sliderArr.length; i++) {
  sliderArr[i].addEventListener("click", function () {
    sliderArr[counter].classList.remove("active");
    sss.innerHTML = "";

    let samePerson = sliderArr[i].cloneNode(true);
    samePerson.style.width = "100rem";
    samePerson.style.height = "100%";
    sliderArr[i].classList.add("active");
    samePerson.style.border = "none";
    counter = i;
    prf.innerText = prof[counter];
    personeName.innerText = personeNames[counter]
    personeText.innerText = personeTexts[counter]

    // console.log(prof[counter]);
    sss.append(samePerson);
    setTimeout(() => (sss.style.transform = "scale(130%)"));
    setTimeout(() => (sss.style.transform = "scale(100%)"), 300);
  });
}

rightBtn.addEventListener("click", () => {
  // prf.innerHTML = "";
  sliderArr[counter].classList.remove("active");
  sss.innerHTML = "";
  counter++;
  if (counter === sliderArr.length) {
    counter = 0;
  }
  let samePerson = sliderArr[counter].cloneNode(true);
  activePersone.append(samePerson);
  samePerson.style.width = "100rem";
  samePerson.style.height = "100%";
  sliderArr[counter].classList.add("active");
  prf.innerText = prof[counter];
  personeName.innerText = personeNames[counter]
  personeText.innerText = personeTexts[counter]



  samePerson.style.border = "none";
  sss.append(samePerson);
  setTimeout(() => (sss.style.transform = "scale(130%)"));
  setTimeout(() => (sss.style.transform = "scale(100%)"), 300);
});

lefttBtn.addEventListener("click", () => {
  sliderArr[counter].classList.remove("active");
  sss.style.transform = "scale(90%)";

  sss.innerHTML = "";
  counter--;
  if (counter < 0) {
    counter = sliderArr.length - 1;
  }
  let samePerson = sliderArr[counter].cloneNode(true);
  sliderArr[counter].classList.add("activ__persone");
  sss.append(samePerson);
  samePerson.style.width = "100rem";
  samePerson.style.height = "100%";
  prf.innerText = prof[counter];
  personeName.innerText = personeNames[counter]
  personeText.innerText = personeTexts[counter]


  sliderArr[counter].classList.add("active");
  samePerson.style.border = "none";
  setTimeout(() => (sss.style.transform = "scale(130%)"));
  setTimeout(() => (sss.style.transform = "scale(100%)"), 300);
});

